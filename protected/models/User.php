<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $dob
 * @property string $username
 * @property string $password
 * @property string $gender
 * @property string $created_on
 * @property string $updated_on
 * @property string $power
 *
 * The followings are the available model relations:
 * @property Complaints[] $complaints
 */
class User extends CActiveRecord
{

    const USER_MALE = 0;
    const USER_FEMALE = 1;

    const USER_STANDARD = 0;
    const USER_ADMIN = 1;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('firstname, lastname, email, username, password, gender', 'required'),
			array('firstname, lastname, email, password', 'length', 'max'=>200),
			array('username', 'length', 'max'=>45),
            array('username , email','unique'),
			array('gender', 'length', 'max'=>1),
			array('dob, created_on, updated_on', 'safe'),
            array('email', 'email'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, firstname, lastname, email, dob, username, password, gender, created_on, updated_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'complaints' => array(self::HAS_MANY, 'Complaints', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'email' => 'Email',
			'dob' => 'Dob',
			'username' => 'Username',
			'password' => 'Password',
			'gender' => 'Gender',
			'created_on' => 'Created On',
			'updated_on' => 'Updated On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('dob',$this->dob,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('updated_on',$this->updated_on,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Generate a random salt in the crypt(3) standard Blowfish format.
     *
     * @param int $cost Cost parameter from 4 to 31.
     *
     * @throws Exception on invalid cost parameter.
     * @return string A Blowfish hash salt for use in PHP's crypt()
     */
    function blowfishSalt($cost = 5)
    {
        if (!is_numeric($cost) || $cost < 4 || $cost > 31) {
            throw new Exception("cost parameter must be between 4 and 31");
        }
        $rand = array();
        for ($i = 0; $i < 8; $i += 1) {
            $rand[] = pack('S', mt_rand(0, 0xffff));
        }
        $rand[] = substr(microtime(), 2, 6);
        $rand = sha1(implode('', $rand), true);
        $salt = '$2a$' . sprintf('%02d', $cost) . '$';
        $salt .= strtr(substr(base64_encode($rand), 0, 22), array('+' => '.'));
        return $salt;
    }

    /**
     * Hashes the password with Blowfish Algorithm and using Crypt.
     * @param  string $password
     * @return string
     */
    public function hashPassword($password)
    {
        return crypt($password,self::blowfishSalt());
    }

    /**
     * Saving the setting after the form has been validated.
     * @return otherthings
     * @author Sankalp <sankalpsingha@gmail.com>
     */
    protected function afterValidate()
    {
        //parent::afterValidate(); // So that the parent can handle if things go wrong.
        if(!$this->hasErrors()){
            $this->password = $this->hashPassword($this->password);
        }
        return true;
    }


    /**
     * This would automatically set the Created On and the Update On
     * attribute for the timestamp
     * @return array
     */
    public function behaviors()
    {
        return array('CTimestampBehavior'=>array(
            'class' => 'zii.behaviors.CTimestampBehavior',
            'createAttribute' => 'created_on',
            'updateAttribute' => 'updated_on',
            'setUpdateOnCreate' => true,
        ));
    }

    /**
     * Gets the user gender in the view.
     * @return array
     * @author <sankalp>
     */
    public function getUserGender()
    {
        return array(self::USER_MALE=>'Male',self::USER_FEMALE=>'Female');
    }

    public function getUserPower()
    {
        return array(
            self::USER_STANDARD => "Standard",
            self::USER_ADMIN => "Admin",
        );
    }

    /**
     * @param $time time
     * @return bool|int|string
     * @author Jagjot Singh <parry@ymail.com>
     */
    public function getElapsedTime($time){
        $return = (int)round((strtotime("now") - strtotime($time))/60, 1);
        if($return < 60){
            if($return <= 1){
                $return .= " minute ago";
            }else{
                $return .= " minutes ago";
            }
        }elseif(($return>=60) && ($return<720)){
            $return = (int)round($return/60);
            if($return<=1){
                $return .= " hour ago";
            }else{
                $return .= " hours ago";
            }
        }elseif($return>720){
            $return = date('l \a\t g\:ia', strtotime($time));
        }
        return $return;
    }
}