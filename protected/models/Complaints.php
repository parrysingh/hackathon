<?php

/**
 * This is the model class for table "complaints".
 *
 * The followings are the available columns in table 'complaints':
 * @property integer $id
 * @property string $title
 * @property string $complaint
 * @property string $created_on
 * @property string $updated_on
 * @property integer $user_id
 * @property string $dept
 *
 * The followings are the available model relations:
 * @property User $user
 */
class Complaints extends CActiveRecord
{
    const MOBILE = 1;
    const COMPUTER = 2;
    const LAPTOP = 3;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Complaints the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'complaints';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, complaint, created_on, updated_on, user_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>100),
            array('dept', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, complaint, created_on, updated_on, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'complaint' => 'Complaint',
			'created_on' => 'Created On',
			'updated_on' => 'Updated On',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('complaint',$this->complaint,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('updated_on',$this->updated_on,true);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * This would automatically set the Created On and the Update On
     * attribute for the timestamp
     * @return array
     */
    public function behaviors()
    {
        return array('CTimestampBehavior'=>array(
            'class' => 'zii.behaviors.CTimestampBehavior',
            'createAttribute' => 'created_on',
            'updateAttribute' => 'updated_on',
            'setUpdateOnCreate' => true,
        ));
    }

    public function getDept(){
        return array(
            self::MOBILE => "Mobile",
            self::LAPTOP => "Laptop",
            self::COMPUTER => "Computer",
        );
    }
}