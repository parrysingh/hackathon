<div class="container">

    <div class="row-fluid">
        <div class="span6 offset3">
            <div class="well">
                <legend>Please Login</legend>
                <div class="form">
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'login-form',
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                    )); ?>
                    <?php

                    // This is for the flash messages
                    $this->widget('bootstrap.widgets.TbAlert', array(
                        'block'=>true, // display a larger alert block?
                        'fade'=>true, // use transitions?
                        'closeText'=>'×', // close link text - if set to false, no close link is displayed
                        'alerts'=>array( // configurations per alert type
                            'contact'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'), // success, info, warning, error or danger
                        ),
                        'htmlOptions' => array('id' => 'myAlert'),
                    ));
                    ?>
                    <p class="note">Fields with <span class="required">*</span> are required.</p>


                    <div class="row-fluid">
                        <?php echo $form->labelEx($model,'username'); ?>
                        <?php echo $form->textField($model,'username'); ?>
                        <?php echo $form->error($model,'username'); ?>
                    </div>

                    <div class="row-fluid">
                        <?php echo $form->labelEx($model,'password'); ?>
                        <?php echo $form->passwordField($model,'password'); ?>
                        <?php echo $form->error($model,'password'); ?>
                        <p class="hint">
                            Hint: You may login with <kbd>demo</kbd>/<kbd>demo</kbd> or <kbd>admin</kbd>/<kbd>admin</kbd>.
                        </p>
                    </div>

                    <div class="row-fluid rememberMe">
                        <?php echo $form->checkBox($model,'rememberMe'); ?>
                        <?php echo $form->label($model,'rememberMe'); ?>
                        <?php echo $form->error($model,'rememberMe'); ?>
                    </div>

                    <div class="row-fluid buttons">
                        <?php echo CHtml::submitButton('Login',array('class'=>'btn btn-primary')); ?>
                    </div>
                    <div class="row-fluid">
                        <?php echo CHtml::link('Register', $this->createUrl('user/create'),array('class'=>' pull-right btn btn-mini btn-success')); ?>
                    </div>


                    <?php $this->endWidget(); ?>
                </div><!-- form -->
            </div>
        </div>
    </div>
</div>