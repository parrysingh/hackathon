

<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>

<?php

$this->widget(
    'bootstrap.widgets.TbNavbar',
    array(
        'type' => 'inverse',
        'brand' => 'Website Making Competition',
        'brandUrl' => '#',
        'collapse' => true, // requires bootstrap-responsive.css
        'fixed' => false,
        'items' => array(
            array(
                'class' => 'bootstrap.widgets.TbMenu',
                'items' => array(
                    array('label' => 'Home', 'url' => $this->createUrl('/complaints'), 'active' => true),
                    array('label' => 'Admin', 'url' => $this->createUrl('/user/admin')),
                ),
            ),
        ),
    )
);
?>
<div class="container">
    <?php
    if(Yii::app()->user->isGuest){
        echo CHtml::link('Log-in', $this->createUrl('/site/login'), array('class' => 'btn btn-success'));
    }else
        echo CHtml::link("Log-out", $this->createUrl('/site/logout'), array('class' => 'btn btn-danger'));
    ?>
    <?php echo $content; ?>
</div>
</body>
</html>