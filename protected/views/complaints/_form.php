<?php
/* @var $this ComplaintsController */
/* @var $model Complaints */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'complaints-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'complaint'); ?>
		<?php echo $form->textArea($model,'complaint',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'complaint'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'dept'); ?>
        <?php echo $form->dropDownList($model,'dept',$model->getDept()); ?>
        <?php echo $form->error($model,'dept'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->