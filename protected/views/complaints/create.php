<?php
/* @var $this ComplaintsController */
/* @var $model Complaints */

$this->breadcrumbs=array(
	'Complaints'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Complaints', 'url'=>array('index')),
	array('label'=>'Manage Complaints', 'url'=>array('admin')),
);
?>

<h1>Create Complaints</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>