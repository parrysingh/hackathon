<?php
/* @var $this ComplaintsController */
/* @var $model Complaints */

$this->breadcrumbs=array(
	'Complaints'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Complaints', 'url'=>array('index')),
	array('label'=>'Create Complaints', 'url'=>array('create')),
);
?>
<?php if(Yii::app()->user->hasFlash('complaint')): ?>

    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('complaint'); ?>
    </div>

<?php endif; ?>
<h1>View Complaints #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'complaint',
		'created_on',
		'updated_on',
		'user_id',
	),
)); ?>
