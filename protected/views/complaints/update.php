<?php
/* @var $this ComplaintsController */
/* @var $model Complaints */

$this->breadcrumbs=array(
	'Complaints'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Complaints', 'url'=>array('index')),
	array('label'=>'Create Complaints', 'url'=>array('create')),
	array('label'=>'View Complaints', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Complaints', 'url'=>array('admin')),
);
?>

<h1>Update Complaints <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>