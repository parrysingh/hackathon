<div class="container">
    <div class="row-fluid">
        <div class="span2">
            <?php
            echo CHtml::link('Create Complaints', array('create'), array('class' => 'btn btn-success'));
            $user = User::model()->findByPk(Yii::app()->user->id);
            if($user->power == 1){

            }
            ?>
        </div>
        <div class="span8 offset1">
            <h1>Complaints</h1>
            <table class="table table-striped table-hover">
                <tr>
                    <td>ID</td>
                    <td>Title</td>
                    <td>Complaint</td>
                    <td>Created On</td>
                    <td>User</td>
                </tr>
                <?php
                foreach ($complaints as $complaint) {
                    ?>
                    <tr>
                        <td>
                            <?php echo CHtml::link($complaint->id, $this->createUrl('/complaints/view/'.$complaint->id)); ?>
                        </td>
                        <td>
                            <?php echo $complaint->title; ?>
                        </td>
                        <td>
                            <?php echo $complaint->complaint; ?>
                        </td>
                        <td>
                            <?php echo User::model()->getElapsedTime($complaint->created_on); ?>
                        </td>
                        <td>
                            <?php echo $complaint->user->firstname; ?>
                        </td>
                    </tr>
                <?php
                }

                ?>
            </table>
        </div>

    </div>
</div>



