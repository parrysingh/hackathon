<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row-fluid">
		<?php echo $form->textFieldRow($model,'firstname',array('size'=>60,'maxlength'=>200,'class'=>'span12')); ?>
		<?php echo $form->error($model,'firstname'); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->textFieldRow($model,'lastname',array('size'=>60,'maxlength'=>200,'class'=>'span12')); ?>
		<?php echo $form->error($model,'lastname'); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->textFieldRow($model,'email',array('size'=>60,'maxlength'=>200,'class'=>'span12')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>


	<div class="row-fluid">
		<?php echo $form->textFieldRow($model,'username',array('size'=>45,'maxlength'=>45,'class'=>'span12')); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->passwordFieldRow($model,'password',array('size'=>60,'maxlength'=>200,'class'=>'span12')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->dropDownListRow($model,'gender',$model->getUserGender()); ?>
		<?php echo $form->error($model,'gender'); ?>
	</div>

	<div class="row-fluid buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->